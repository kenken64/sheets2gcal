console.log("Sync from Google sheets to GCal");
const GoogleSpreadsheet = require('google-spreadsheet');
const async =  require('async');

const googleSheetId = process.env.G_SHEET_ID;
const googleClientEmail = process.env.G_MAIL;
const googlePrivKey = process.env.G_PRIV_KEY;

console.log('googleSheetId > ' , googleSheetId);

var doc = new GoogleSpreadsheet(googleSheetId);
var sheet;


async.series([

	function setAuth(step){
		console.log(step);
		var creds_json = {
			client_email: googleClientEmail,
			private_key: googlePrivKey
		}
		doc.useServiceAccountAuth(creds_json, step);		
	},
	function getInfoAndWorksheets(step){
		console.log('get info of the worksheets');
		
	}
	
]);
